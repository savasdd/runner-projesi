#FROM openjdk:8-jdk-alpine
#FROM openjdk:8-jre
#FROM openjdk:17-oracle
FROM openjdk:11-jdk-alpine
ADD target/RunnerApi-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
